/* jslint node:true */

'use strict';

var argv = require('yargs').argv,
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    cssnano = require('gulp-cssnano'),
    ejs = require('gulp-ejs'),
    execSync = require('child_process').execSync,
    gulp = require('gulp'),
    rimraf = require('rimraf'),
    sass = require('gulp-sass'),
    serve = require('gulp-serve'),
    sourcemaps = require('gulp-sourcemaps');

if (argv.help || argv.h) {
    console.log('Supported arguments for "gulp develop":');
    console.log(' --api-origin <cloudron api uri>');
    console.log(' --revision <revision>');
    console.log(' --appstore-web-origin <appstore web uri>');
    console.log(' --appstore-api-origin <appstore api uri>');

    process.exit(1);
}

const revision = argv.revision || '';

let apiOrigin = '';
if (argv.apiOrigin) {
    if (argv.apiOrigin.indexOf('https://') === 0) apiOrigin = argv.apiOrigin;
    else apiOrigin = 'https://' + argv.apiOrigin;
}

var appstore = {
    webOrigin: argv.appstoreWebOrigin || '',
    apiOrigin: argv.appstoreApiOrigin || ''
}

console.log();
console.log('Cloudron API:          %s', apiOrigin || 'default');
console.log('Building for revision: %s', revision);
console.log();
console.log('Overriding appstore origin:');
console.log(' Website: %s', appstore.webOrigin || 'no');
console.log(' Api:     %s', appstore.apiOrigin || 'no');
console.log();

gulp.task('fontawesome', function () {
    return gulp.src([
        'node_modules/@fortawesome/fontawesome-free/*css*/all.min.css',
        'node_modules/@fortawesome/fontawesome-free/*webfonts*/*.eot',
        'node_modules/@fortawesome/fontawesome-free/*webfonts*/*.svg',
        'node_modules/@fortawesome/fontawesome-free/*webfonts*/*.ttf',
        'node_modules/@fortawesome/fontawesome-free/*webfonts*/*.woff',
        'node_modules/@fortawesome/fontawesome-free/*webfonts*/*.woff2'
    ]).pipe(gulp.dest('dist/3rdparty/fontawesome/'));
});

gulp.task('bootstrap', function () {
    return gulp.src('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js')
        .pipe(gulp.dest('dist/3rdparty/js'));
});

gulp.task('monaco', function () {
    return gulp.src('node_modules/monaco-editor/min/**/*')
        .pipe(gulp.dest('dist/3rdparty/'));
});

gulp.task('3rdparty-copy', function () {
    return gulp.src([
        'src/3rdparty/**/*.js',
        'src/3rdparty/**/*.map',
        'src/3rdparty/**/*.css',
        'src/3rdparty/**/*.otf',
        'src/3rdparty/**/*.eot',
        'src/3rdparty/**/*.svg',
        'src/3rdparty/**/*.gif',
        'src/3rdparty/**/*.ttf'
        ])
        .pipe(gulp.dest('dist/3rdparty/'));
});

gulp.task('3rdparty', gulp.series(['3rdparty-copy', 'monaco', 'bootstrap', 'fontawesome']));

// --------------
// JavaScript
// --------------

gulp.task('js-index', function () {
    return gulp.src([
        'src/js/index.js',
        'src/js/client.js',
        'src/js/main.js',
        'src/views/*.js'
        ])
        .pipe(ejs({ apiOrigin: apiOrigin, revision: revision, appstore: appstore }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('index.js', { newLine: ';' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-logs', function () {
    return gulp.src(['src/js/logs.js', 'src/js/client.js'])
        .pipe(ejs({ apiOrigin: apiOrigin, appstore: appstore }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('logs.js', { newLine: ';' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-filemanager', function () {
    return gulp.src(['src/js/filemanager.js', 'src/js/client.js'])
        .pipe(ejs({ apiOrigin: apiOrigin, appstore: appstore }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('filemanager.js', { newLine: ';' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-terminal', function () {
    return gulp.src(['src/js/terminal.js', 'src/js/client.js'])
        .pipe(ejs({ apiOrigin: apiOrigin, appstore: appstore }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('terminal.js', { newLine: ';' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-login', function () {
    return gulp.src(['src/js/login.js'])
        .pipe(ejs({ apiOrigin: apiOrigin, appstore: appstore }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('login.js', { newLine: ';' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-setupaccount', function () {
    return gulp.src(['src/js/setupaccount.js'])
        .pipe(ejs({ apiOrigin: apiOrigin, appstore: appstore }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('setupaccount.js', { newLine: ';' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-setup', function () {
    return gulp.src(['src/js/setup.js', 'src/js/client.js'])
        .pipe(ejs({ apiOrigin: apiOrigin, appstore: appstore }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('setup.js', { newLine: ';' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-setupdns', function () {
    return gulp.src(['src/js/setupdns.js', 'src/js/client.js'])
        .pipe(ejs({ apiOrigin: apiOrigin, appstore: appstore }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('setupdns.js', { newLine: ';' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-restore', function () {
    return gulp.src(['src/js/restore.js', 'src/js/client.js'])
        .pipe(ejs({ apiOrigin: apiOrigin, appstore: appstore }, {}, { ext: '.js' }))
        .pipe(sourcemaps.init())
        .pipe(concat('restore.js', { newLine: ';' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js', gulp.series([ 'js-index', 'js-logs', 'js-filemanager', 'js-terminal', 'js-login', 'js-setupaccount', 'js-setup', 'js-setupdns', 'js-restore' ]));

// --------------
// HTML
// --------------

gulp.task('html-views', function () {
    return gulp.src('src/views/**/*.html').pipe(gulp.dest('dist/views'));
});

gulp.task('html-templates', function () {
    return gulp.src('src/templates/**/*.html').pipe(gulp.dest('dist/templates'));
});

gulp.task('html-raw', function () {
    return gulp.src('src/*.html').pipe(ejs({ apiOrigin: apiOrigin, revision: revision }, {}, { ext: '.html' })).pipe(gulp.dest('dist'));
});

gulp.task('html', gulp.series(['html-views', 'html-templates', 'html-raw']));

// --------------
// CSS
// --------------

gulp.task('css', function () {
    return gulp.src('src/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ includePaths: ['node_modules/bootstrap-sass/assets/stylesheets/'] }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist'));
});

gulp.task('images', function () {
    return gulp.src('src/img/**')
        .pipe(gulp.dest('dist/img'));
});

gulp.task('timezones', function (done) {
    execSync('./scripts/createTimezones.js ./dist/js/timezones.js');
    done();
});

// --------------
// Utilities
// --------------

gulp.task('clean', function (done) {
    rimraf.sync('dist');
    done();
});

gulp.task('default', gulp.series(['clean', 'html', 'js', 'timezones', '3rdparty', 'images', 'css']));

gulp.task('watch', function (done) {
    gulp.watch(['src/*.scss'], gulp.series(['css']));
    gulp.watch(['src/img/*'], gulp.series(['images']));
    gulp.watch(['src/**/*.html'], gulp.series(['html']));
    gulp.watch(['src/views/*.html'], gulp.series(['html-views']));
    gulp.watch(['src/templates/*.html'], gulp.series(['html-templates']));
    gulp.watch(['scripts/createTimezones.js'], gulp.series(['timezones']));
    gulp.watch(['src/js/setup.js', 'src/js/client.js'], gulp.series(['js-setup']));
    gulp.watch(['src/js/setupdns.js', 'src/js/client.js'], gulp.series(['js-setupdns']));
    gulp.watch(['src/js/restore.js', 'src/js/client.js'], gulp.series(['js-restore']));
    gulp.watch(['src/js/logs.js', 'src/js/client.js'], gulp.series(['js-logs']));
    gulp.watch(['src/js/filemanager.js', 'src/js/client.js'], gulp.series(['js-filemanager']));
    gulp.watch(['src/js/terminal.js', 'src/js/client.js'], gulp.series(['js-terminal']));
    gulp.watch(['src/js/login.js'], gulp.series(['js-login']));
    gulp.watch(['src/js/setupaccount.js'], gulp.series(['js-setupaccount']));
    gulp.watch(['src/js/index.js', 'src/js/client.js', 'src/js/main.js', 'src/views/*.js'], gulp.series(['js-index']));
    gulp.watch(['src/3rdparty/**/*'], gulp.series(['3rdparty']));
    done();
});

gulp.task('serve', serve({ root: 'dist', port: 4000 }));

gulp.task('develop', gulp.series(['default', 'watch', 'serve']));

