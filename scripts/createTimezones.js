#!/usr/bin/env node

// This script creates a specific timezones.js to be consumed by the dashboard

var execSync = require('child_process').execSync,
    fs = require('fs'),
    path = require('path');

if (process.argv.length !== 3) {
    console.log('Usage: createTimezones.js <output.js>');
    process.exit(1);
}

const destinationFilePath = path.resolve(process.argv[2]);
console.log('Creating timezone info at:', destinationFilePath);

var ubuntuTimezones = execSync('timedatectl list-timezones --no-pager').toString().split('\n').filter(function (t) { return !!t; });

// from https://github.com/dmfilipenko/timezones.json/blob/master/timezones.json
var details = [
    { name: 'UTC', offset: '+00:00' },
    { name: 'Africa/Abidjan', offset: '+00:00' },
    { name: 'Africa/Accra', offset: '+00:00' },
    { name: 'Africa/Algiers', offset: '+01:00' },
    { name: 'Africa/Bissau', offset: '+00:00' },
    { name: 'Africa/Cairo', offset: '+02:00' },
    { name: 'Africa/Casablanca', offset: '+01:00' },
    { name: 'Africa/Ceuta', offset: '+01:00' },
    { name: 'Africa/El_Aaiun', offset: '+00:00' },
    { name: 'Africa/Johannesburg', offset: '+02:00' },
    { name: 'Africa/Juba', offset: '+03:00' },
    { name: 'Africa/Khartoum', offset: '+02:00' },
    { name: 'Africa/Lagos', offset: '+01:00' },
    { name: 'Africa/Maputo', offset: '+02:00' },
    { name: 'Africa/Monrovia', offset: '+00:00' },
    { name: 'Africa/Nairobi', offset: '+03:00' },
    { name: 'Africa/Ndjamena', offset: '+01:00' },
    { name: 'Africa/Tripoli', offset: '+02:00' },
    { name: 'Africa/Tunis', offset: '+01:00' },
    { name: 'Africa/Windhoek', offset: '+02:00' },
    { name: 'America/Adak', offset: '−10:00' },
    { name: 'America/Anchorage', offset: '−09:00' },
    { name: 'America/Araguaina', offset: '−03:00' },
    { name: 'America/Argentina/Buenos_Aires', offset: '−03:00' },
    { name: 'America/Argentina/Catamarca', offset: '−03:00' },
    { name: 'America/Argentina/Cordoba', offset: '−03:00' },
    { name: 'America/Argentina/Jujuy', offset: '−03:00' },
    { name: 'America/Argentina/La_Rioja', offset: '−03:00' },
    { name: 'America/Argentina/Mendoza', offset: '−03:00' },
    { name: 'America/Argentina/Rio_Gallegos', offset: '−03:00' },
    { name: 'America/Argentina/Salta', offset: '−03:00' },
    { name: 'America/Argentina/San_Juan', offset: '−03:00' },
    { name: 'America/Argentina/San_Luis', offset: '−03:00' },
    { name: 'America/Argentina/Tucuman', offset: '−03:00' },
    { name: 'America/Argentina/Ushuaia', offset: '−03:00' },
    { name: 'America/Asuncion', offset: '−04:00' },
    { name: 'America/Atikokan', offset: '−05:00' },
    { name: 'America/Bahia', offset: '−03:00' },
    { name: 'America/Bahia_Banderas', offset: '−06:00' },
    { name: 'America/Barbados', offset: '−04:00' },
    { name: 'America/Belem', offset: '−03:00' },
    { name: 'America/Belize', offset: '−06:00' },
    { name: 'America/Blanc-Sablon', offset: '−04:00' },
    { name: 'America/Boa_Vista', offset: '−04:00' },
    { name: 'America/Bogota', offset: '−05:00' },
    { name: 'America/Boise', offset: '−07:00' },
    { name: 'America/Cambridge_Bay', offset: '−07:00' },
    { name: 'America/Campo_Grande', offset: '−04:00' },
    { name: 'America/Cancun', offset: '−05:00' },
    { name: 'America/Caracas', offset: '−04:00' },
    { name: 'America/Cayenne', offset: '−03:00' },
    { name: 'America/Chicago', offset: '−06:00' },
    { name: 'America/Chihuahua', offset: '−07:00' },
    { name: 'America/Costa_Rica', offset: '−06:00' },
    { name: 'America/Creston', offset: '−07:00' },
    { name: 'America/Cuiaba', offset: '−04:00' },
    { name: 'America/Curacao', offset: '−04:00' },
    { name: 'America/Danmarkshavn', offset: '+00:00' },
    { name: 'America/Dawson', offset: '−08:00' },
    { name: 'America/Dawson_Creek', offset: '−07:00' },
    { name: 'America/Denver', offset: '−07:00' },
    { name: 'America/Detroit', offset: '−05:00' },
    { name: 'America/Edmonton', offset: '−07:00' },
    { name: 'America/Eirunepe', offset: '−05:00' },
    { name: 'America/El_Salvador', offset: '−06:00' },
    { name: 'America/Fort_Nelson', offset: '−07:00' },
    { name: 'America/Fortaleza', offset: '−03:00' },
    { name: 'America/Glace_Bay', offset: '−04:00' },
    { name: 'America/Godthab', offset: '−03:00' },
    { name: 'America/Goose_Bay', offset: '−04:00' },
    { name: 'America/Grand_Turk', offset: '−05:00' },
    { name: 'America/Guatemala', offset: '−06:00' },
    { name: 'America/Guayaquil', offset: '−05:00' },
    { name: 'America/Guyana', offset: '−04:00' },
    { name: 'America/Halifax', offset: '−04:00' },
    { name: 'America/Havana', offset: '−05:00' },
    { name: 'America/Hermosillo', offset: '−07:00' },
    { name: 'America/Indiana/Indianapolis', offset: '−05:00' },
    { name: 'America/Indiana/Knox', offset: '−06:00' },
    { name: 'America/Indiana/Marengo', offset: '−05:00' },
    { name: 'America/Indiana/Petersburg', offset: '−05:00' },
    { name: 'America/Indiana/Tell_City', offset: '−06:00' },
    { name: 'America/Indiana/Vevay', offset: '−05:00' },
    { name: 'America/Indiana/Vincennes', offset: '−05:00' },
    { name: 'America/Indiana/Winamac', offset: '−05:00' },
    { name: 'America/Inuvik', offset: '−07:00' },
    { name: 'America/Iqaluit', offset: '−05:00' },
    { name: 'America/Jamaica', offset: '−05:00' },
    { name: 'America/Juneau', offset: '−09:00' },
    { name: 'America/Kentucky/Louisville', offset: '−05:00' },
    { name: 'America/Kentucky/Monticello', offset: '−05:00' },
    { name: 'America/La_Paz', offset: '−04:00' },
    { name: 'America/Lima', offset: '−05:00' },
    { name: 'America/Los_Angeles', offset: '−08:00' },
    { name: 'America/Maceio', offset: '−03:00' },
    { name: 'America/Managua', offset: '−06:00' },
    { name: 'America/Manaus', offset: '−04:00' },
    { name: 'America/Martinique', offset: '−04:00' },
    { name: 'America/Matamoros', offset: '−06:00' },
    { name: 'America/Mazatlan', offset: '−07:00' },
    { name: 'America/Menominee', offset: '−06:00' },
    { name: 'America/Merida', offset: '−06:00' },
    { name: 'America/Metlakatla', offset: '−09:00' },
    { name: 'America/Mexico_City', offset: '−06:00' },
    { name: 'America/Miquelon', offset: '−03:00' },
    { name: 'America/Moncton', offset: '−04:00' },
    { name: 'America/Monterrey', offset: '−06:00' },
    { name: 'America/Montevideo', offset: '−03:00' },
    { name: 'America/Nassau', offset: '−05:00' },
    { name: 'America/New_York', offset: '−05:00' },
    { name: 'America/Nipigon', offset: '−05:00' },
    { name: 'America/Nome', offset: '−09:00' },
    { name: 'America/Noronha', offset: '−02:00' },
    { name: 'America/North_Dakota/Beulah', offset: '−06:00' },
    { name: 'America/North_Dakota/Center', offset: '−06:00' },
    { name: 'America/North_Dakota/New_Salem', offset: '−06:00' },
    { name: 'America/Ojinaga', offset: '−07:00' },
    { name: 'America/Panama', offset: '−05:00' },
    { name: 'America/Pangnirtung', offset: '−05:00' },
    { name: 'America/Paramaribo', offset: '−03:00' },
    { name: 'America/Phoenix', offset: '−07:00' },
    { name: 'America/Port_of_Spain', offset: '−04:00' },
    { name: 'America/Port-au-Prince', offset: '−05:00' },
    { name: 'America/Porto_Velho', offset: '−04:00' },
    { name: 'America/Puerto_Rico', offset: '−04:00' },
    { name: 'America/Punta_Arenas', offset: '−03:00' },
    { name: 'America/Rainy_River', offset: '−06:00' },
    { name: 'America/Rankin_Inlet', offset: '−06:00' },
    { name: 'America/Recife', offset: '−03:00' },
    { name: 'America/Regina', offset: '−06:00' },
    { name: 'America/Resolute', offset: '−06:00' },
    { name: 'America/Rio_Branco', offset: '−05:00' },
    { name: 'America/Santarem', offset: '−03:00' },
    { name: 'America/Santiago', offset: '−04:00' },
    { name: 'America/Santo_Domingo', offset: '−04:00' },
    { name: 'America/Sao_Paulo', offset: '−03:00' },
    { name: 'America/Scoresbysund', offset: '−01:00' },
    { name: 'America/Sitka', offset: '−09:00' },
    { name: 'America/St_Johns', offset: '−03:30' },
    { name: 'America/Swift_Current', offset: '−06:00' },
    { name: 'America/Tegucigalpa', offset: '−06:00' },
    { name: 'America/Thule', offset: '−04:00' },
    { name: 'America/Thunder_Bay', offset: '−05:00' },
    { name: 'America/Tijuana', offset: '−08:00' },
    { name: 'America/Toronto', offset: '−05:00' },
    { name: 'America/Vancouver', offset: '−08:00' },
    { name: 'America/Whitehorse', offset: '−08:00' },
    { name: 'America/Winnipeg', offset: '−06:00' },
    { name: 'America/Yakutat', offset: '−09:00' },
    { name: 'America/Yellowknife', offset: '−07:00' },
    { name: 'Antarctica/Casey', offset: '+11:00' },
    { name: 'Antarctica/Davis', offset: '+07:00' },
    { name: 'Antarctica/DumontDUrville', offset: '+10:00' },
    { name: 'Antarctica/Macquarie', offset: '+11:00' },
    { name: 'Antarctica/Mawson', offset: '+05:00' },
    { name: 'Antarctica/Palmer', offset: '−03:00' },
    { name: 'Antarctica/Rothera', offset: '−03:00' },
    { name: 'Antarctica/Syowa', offset: '+03:00' },
    { name: 'Antarctica/Troll', offset: '+00:00' },
    { name: 'Antarctica/Vostok', offset: '+06:00' },
    { name: 'Asia/Almaty', offset: '+06:00' },
    { name: 'Asia/Amman', offset: '+02:00' },
    { name: 'Asia/Anadyr', offset: '+12:00' },
    { name: 'Asia/Aqtau', offset: '+05:00' },
    { name: 'Asia/Aqtobe', offset: '+05:00' },
    { name: 'Asia/Ashgabat', offset: '+05:00' },
    { name: 'Asia/Atyrau', offset: '+05:00' },
    { name: 'Asia/Baghdad', offset: '+03:00' },
    { name: 'Asia/Baku', offset: '+04:00' },
    { name: 'Asia/Bangkok', offset: '+07:00' },
    { name: 'Asia/Barnaul', offset: '+07:00' },
    { name: 'Asia/Beirut', offset: '+02:00' },
    { name: 'Asia/Bishkek', offset: '+06:00' },
    { name: 'Asia/Brunei', offset: '+08:00' },
    { name: 'Asia/Chita', offset: '+09:00' },
    { name: 'Asia/Choibalsan', offset: '+08:00' },
    { name: 'Asia/Colombo', offset: '+05:30' },
    { name: 'Asia/Damascus', offset: '+02:00' },
    { name: 'Asia/Dhaka', offset: '+06:00' },
    { name: 'Asia/Dili', offset: '+09:00' },
    { name: 'Asia/Dubai', offset: '+04:00' },
    { name: 'Asia/Dushanbe', offset: '+05:00' },
    { name: 'Asia/Famagusta', offset: '+02:00' },
    { name: 'Asia/Gaza', offset: '+02:00' },
    { name: 'Asia/Hebron', offset: '+02:00' },
    { name: 'Asia/Ho_Chi_Minh', offset: '+07:00' },
    { name: 'Asia/Hong_Kong', offset: '+08:00' },
    { name: 'Asia/Hovd', offset: '+07:00' },
    { name: 'Asia/Irkutsk', offset: '+08:00' },
    { name: 'Asia/Jakarta', offset: '+07:00' },
    { name: 'Asia/Jayapura', offset: '+09:00' },
    { name: 'Asia/Jerusalem', offset: '+02:00' },
    { name: 'Asia/Kabul', offset: '+04:30' },
    { name: 'Asia/Kamchatka', offset: '+12:00' },
    { name: 'Asia/Karachi', offset: '+05:00' },
    { name: 'Asia/Kathmandu', offset: '+05:45' },
    { name: 'Asia/Khandyga', offset: '+09:00' },
    { name: 'Asia/Kolkata', offset: '+05:30' },
    { name: 'Asia/Krasnoyarsk', offset: '+07:00' },
    { name: 'Asia/Kuala_Lumpur', offset: '+08:00' },
    { name: 'Asia/Kuching', offset: '+08:00' },
    { name: 'Asia/Macau', offset: '+08:00' },
    { name: 'Asia/Magadan', offset: '+11:00' },
    { name: 'Asia/Makassar', offset: '+08:00' },
    { name: 'Asia/Manila', offset: '+08:00' },
    { name: 'Asia/Novokuznetsk', offset: '+07:00' },
    { name: 'Asia/Novosibirsk', offset: '+07:00' },
    { name: 'Asia/Omsk', offset: '+06:00' },
    { name: 'Asia/Oral', offset: '+05:00' },
    { name: 'Asia/Pontianak', offset: '+07:00' },
    { name: 'Asia/Pyongyang', offset: '+09:00' },
    { name: 'Asia/Qatar', offset: '+03:00' },
    { name: 'Asia/Qyzylorda', offset: '+05:00' },
    { name: 'Asia/Riyadh', offset: '+03:00' },
    { name: 'Asia/Sakhalin', offset: '+11:00' },
    { name: 'Asia/Samarkand', offset: '+05:00' },
    { name: 'Asia/Seoul', offset: '+09:00' },
    { name: 'Asia/Shanghai', offset: '+08:00' },
    { name: 'Asia/Singapore', offset: '+08:00' },
    { name: 'Asia/Srednekolymsk', offset: '+11:00' },
    { name: 'Asia/Taipei', offset: '+08:00' },
    { name: 'Asia/Tashkent', offset: '+05:00' },
    { name: 'Asia/Tbilisi', offset: '+04:00' },
    { name: 'Asia/Tehran', offset: '+03:30' },
    { name: 'Asia/Thimphu', offset: '+06:00' },
    { name: 'Asia/Tokyo', offset: '+09:00' },
    { name: 'Asia/Tomsk', offset: '+07:00' },
    { name: 'Asia/Ulaanbaatar', offset: '+08:00' },
    { name: 'Asia/Urumqi', offset: '+06:00' },
    { name: 'Asia/Ust-Nera', offset: '+10:00' },
    { name: 'Asia/Vladivostok', offset: '+10:00' },
    { name: 'Asia/Yakutsk', offset: '+09:00' },
    { name: 'Asia/Yangon', offset: '+06:30' },
    { name: 'Asia/Yekaterinburg', offset: '+05:00' },
    { name: 'Asia/Yerevan', offset: '+04:00' },
    { name: 'Atlantic/Azores', offset: '−01:00' },
    { name: 'Atlantic/Bermuda', offset: '−04:00' },
    { name: 'Atlantic/Canary', offset: '+00:00' },
    { name: 'Atlantic/Cape_Verde', offset: '−01:00' },
    { name: 'Atlantic/Faroe', offset: '+00:00' },
    { name: 'Atlantic/Madeira', offset: '+00:00' },
    { name: 'Atlantic/Reykjavik', offset: '+00:00' },
    { name: 'Atlantic/South_Georgia', offset: '−02:00' },
    { name: 'Atlantic/Stanley', offset: '−03:00' },
    { name: 'Australia/Adelaide', offset: '+09:30' },
    { name: 'Australia/Brisbane', offset: '+10:00' },
    { name: 'Australia/Broken_Hill', offset: '+09:30' },
    { name: 'Australia/Currie', offset: '+10:00' },
    { name: 'Australia/Darwin', offset: '+09:30' },
    { name: 'Australia/Eucla', offset: '+08:45' },
    { name: 'Australia/Hobart', offset: '+10:00' },
    { name: 'Australia/Lindeman', offset: '+10:00' },
    { name: 'Australia/Lord_Howe', offset: '+10:30' },
    { name: 'Australia/Melbourne', offset: '+10:00' },
    { name: 'Australia/Perth', offset: '+08:00' },
    { name: 'Australia/Sydney', offset: '+10:00' },
    { name: 'Europe/Amsterdam', offset: '+01:00' },
    { name: 'Europe/Andorra', offset: '+01:00' },
    { name: 'Europe/Astrakhan', offset: '+04:00' },
    { name: 'Europe/Athens', offset: '+02:00' },
    { name: 'Europe/Belgrade', offset: '+01:00' },
    { name: 'Europe/Berlin', offset: '+01:00' },
    { name: 'Europe/Brussels', offset: '+01:00' },
    { name: 'Europe/Bucharest', offset: '+02:00' },
    { name: 'Europe/Budapest', offset: '+01:00' },
    { name: 'Europe/Chisinau', offset: '+02:00' },
    { name: 'Europe/Copenhagen', offset: '+01:00' },
    { name: 'Europe/Dublin', offset: '+00:00' },
    { name: 'Europe/Gibraltar', offset: '+01:00' },
    { name: 'Europe/Helsinki', offset: '+02:00' },
    { name: 'Europe/Istanbul', offset: '+03:00' },
    { name: 'Europe/Kaliningrad', offset: '+02:00' },
    { name: 'Europe/Kiev', offset: '+02:00' },
    { name: 'Europe/Kirov', offset: '+03:00' },
    { name: 'Europe/Lisbon', offset: '+00:00' },
    { name: 'Europe/London', offset: '+00:00' },
    { name: 'Europe/Luxembourg', offset: '+01:00' },
    { name: 'Europe/Madrid', offset: '+01:00' },
    { name: 'Europe/Malta', offset: '+01:00' },
    { name: 'Europe/Minsk', offset: '+03:00' },
    { name: 'Europe/Monaco', offset: '+01:00' },
    { name: 'Europe/Moscow', offset: '+03:00' },
    { name: 'Asia/Nicosia', offset: '+02:00' },
    { name: 'Europe/Oslo', offset: '+01:00' },
    { name: 'Europe/Paris', offset: '+01:00' },
    { name: 'Europe/Prague', offset: '+01:00' },
    { name: 'Europe/Riga', offset: '+02:00' },
    { name: 'Europe/Rome', offset: '+01:00' },
    { name: 'Europe/Samara', offset: '+04:00' },
    { name: 'Europe/Saratov', offset: '+04:00' },
    { name: 'Europe/Simferopol', offset: '+03:00' },
    { name: 'Europe/Sofia', offset: '+02:00' },
    { name: 'Europe/Stockholm', offset: '+01:00' },
    { name: 'Europe/Tallinn', offset: '+02:00' },
    { name: 'Europe/Tirane', offset: '+01:00' },
    { name: 'Europe/Ulyanovsk', offset: '+04:00' },
    { name: 'Europe/Uzhgorod', offset: '+02:00' },
    { name: 'Europe/Vienna', offset: '+01:00' },
    { name: 'Europe/Vilnius', offset: '+02:00' },
    { name: 'Europe/Volgograd', offset: '+04:00' },
    { name: 'Europe/Warsaw', offset: '+01:00' },
    { name: 'Europe/Zaporozhye', offset: '+02:00' },
    { name: 'Europe/Zurich', offset: '+01:00' },
    { name: 'Indian/Chagos', offset: '+06:00' },
    { name: 'Indian/Christmas', offset: '+07:00' },
    { name: 'Indian/Cocos', offset: '+06:30' },
    { name: 'Indian/Kerguelen', offset: '+05:00' },
    { name: 'Indian/Mahe', offset: '+04:00' },
    { name: 'Indian/Maldives', offset: '+05:00' },
    { name: 'Indian/Mauritius', offset: '+04:00' },
    { name: 'Indian/Reunion', offset: '+04:00' },
    { name: 'Pacific/Apia', offset: '+13:00' },
    { name: 'Pacific/Auckland', offset: '+12:00' },
    { name: 'Pacific/Bougainville', offset: '+11:00' },
    { name: 'Pacific/Chatham', offset: '+12:45' },
    { name: 'Pacific/Chuuk', offset: '+10:00' },
    { name: 'Pacific/Easter', offset: '−06:00' },
    { name: 'Pacific/Efate', offset: '+11:00' },
    { name: 'Pacific/Enderbury', offset: '+13:00' },
    { name: 'Pacific/Fakaofo', offset: '+13:00' },
    { name: 'Pacific/Fiji', offset: '+12:00' },
    { name: 'Pacific/Funafuti', offset: '+12:00' },
    { name: 'Pacific/Galapagos', offset: '−06:00' },
    { name: 'Pacific/Gambier', offset: '−09:00' },
    { name: 'Pacific/Guadalcanal', offset: '+11:00' },
    { name: 'Pacific/Guam', offset: '+10:00' },
    { name: 'Pacific/Honolulu', offset: '−10:00' },
    { name: 'Pacific/Kiritimati', offset: '+14:00' },
    { name: 'Pacific/Kosrae', offset: '+11:00' },
    { name: 'Pacific/Kwajalein', offset: '+12:00' },
    { name: 'Pacific/Majuro', offset: '+12:00' },
    { name: 'Pacific/Marquesas', offset: '−09:30' },
    { name: 'Pacific/Nauru', offset: '+12:00' },
    { name: 'Pacific/Niue', offset: '−11:00' },
    { name: 'Pacific/Norfolk', offset: '+11:00' },
    { name: 'Pacific/Noumea', offset: '+11:00' },
    { name: 'Pacific/Pago_Pago', offset: '−11:00' },
    { name: 'Pacific/Palau', offset: '+09:00' },
    { name: 'Pacific/Pitcairn', offset: '−08:00' },
    { name: 'Pacific/Pohnpei', offset: '+11:00' },
    { name: 'Pacific/Port_Moresby', offset: '+10:00' },
    { name: 'Pacific/Rarotonga', offset: '−10:00' },
    { name: 'Pacific/Tahiti', offset: '−10:00' },
    { name: 'Pacific/Tarawa', offset: '+12:00' },
    { name: 'Pacific/Tongatapu', offset: '+13:00' },
    { name: 'Pacific/Wake', offset: '+12:00' },
    { name: 'Pacific/Wallis', offset: '+12:00' },
];

var timezones = ubuntuTimezones.map(function (t) {
    var detail = details.find(function (d) {
        return d.name === t;
    });

    if (!detail) return null;

    return {
        id: t,
        display: `${t} (UTC${detail.offset})`
    }
}).filter(function (t) { return !!t; });

var output = `(function () { window.timezones = ${JSON.stringify(timezones)}; })();\n`;

fs.writeFileSync(destinationFilePath, output, 'utf-8');

console.log('Done');
