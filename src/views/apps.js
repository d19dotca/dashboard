'use strict';

/* global angular:false */
/* global $:false */

angular.module('Application').controller('AppsController', ['$scope', '$timeout', '$interval', '$location', 'Client', function ($scope, $timeout, $interval, $location, Client) {
    var ALL_DOMAINS_DOMAIN = { _alldomains: true, domain: 'All Domains' }; // dummy record for the single select filter

    $scope.installedApps = Client.getInstalledApps();
    $scope.tags = Client.getAppTags();
    $scope.selectedTags = [];
    $scope.selectedDomain = ALL_DOMAINS_DOMAIN;
    $scope.filterDomains = [ ALL_DOMAINS_DOMAIN ];
    $scope.config = Client.getConfig();
    $scope.user = Client.getUserInfo();
    $scope.domains = [];
    $scope.appSearch = '';

    $scope.$watch('selectedTags', function (newVal, oldVal) {
        if (newVal === oldVal) return;

        localStorage.selectedTags = newVal.join(',');
    });

    $scope.$watch('selectedDomain', function (newVal, oldVal) {
        if (newVal === oldVal) return;

        if (newVal._alldomains) localStorage.removeItem('selectedDomain');
        else localStorage.selectedDomain = newVal.domain;
    });

    $scope.appPostInstallConfirm = {
        app: {},
        message: '',
        confirmed: false,
        customAuth: false,

        show: function (app) {
            $scope.appPostInstallConfirm.app = app;
            $scope.appPostInstallConfirm.message = app.manifest.postInstallMessage;
            $scope.appPostInstallConfirm.confirmed = false;
            $scope.appPostInstallConfirm.customAuth = !(app.manifest.addons['ldap'] || app.manifest.addons['oauth']);

            $('#appPostInstallConfirmModal').modal('show');

            return false; // prevent propagation and default
        },

        submit: function () {
            if (!$scope.appPostInstallConfirm.confirmed) return;

            $scope.appPostInstallConfirm.app.pendingPostInstallConfirmation = false;
            delete localStorage['confirmPostInstall_' + $scope.appPostInstallConfirm.app.id];

            $('#appPostInstallConfirmModal').modal('hide');
        }
    };

    $scope.appInfo = {
        app: {},
        message: '',
        customAuth: false,

        show: function (app) {
            $scope.appInfo.app = app;
            $scope.appInfo.message = app.manifest.postInstallMessage;
            $scope.appInfo.customAuth = !(app.manifest.addons['ldap'] || app.manifest.addons['oauth']);

            $('#appinfoPostinstallMessage').collapse('hide');
            $('#appInfoModal').modal('show');

            return false; // prevent propagation and default
        }
    };

    $scope.showAppConfigure = function (app, view) {
        $location.path('/app/' + app.id + '/' + view);
    };

    Client.onReady(function () {
        setTimeout(function () { $('#appSearch').focus(); }, 1);

        // refresh the new list immediately when switching from another view (appstore)
        Client.refreshInstalledApps(function () {
            var refreshAppsTimer = $interval(Client.refreshInstalledApps.bind(Client, function () {}), 5000);
            $scope.$on('$destroy', function () {
                $interval.cancel(refreshAppsTimer);
            });
        });

        if (!$scope.user.isAtLeastAdmin) return;

        // load local settings and apply tag filter
        if (localStorage.selectedTags) {
            if (!$scope.tags.length) localStorage.removeItem('selectedTags');
            else $scope.selectedTags = localStorage.selectedTags.split(',');
        }

        Client.getDomains(function (error, result) {
            if (error) Client.error(error);

            $scope.domains = result;
            $scope.filterDomains = [ALL_DOMAINS_DOMAIN].concat(result);

            if (localStorage.selectedDomain) $scope.selectedDomain = $scope.filterDomains.find(function (d) { return d.domain === localStorage.selectedDomain; }) || ALL_DOMAINS_DOMAIN;
        });
    });

    $('.collapse').on('shown.bs.collapse', function(){
        $(this).parent().find('.fa-angle-right').removeClass('fa-angle-right').addClass('fa-angle-down');
    }).on('hidden.bs.collapse', function(){
        $(this).parent().find('.fa-angle-down').removeClass('fa-angle-down').addClass('fa-angle-right');
    });

    $('.modal-backdrop').remove();
}]);
