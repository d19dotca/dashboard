'use strict';

/* global angular:false */
/* global $:false */

angular.module('Application').controller('SettingsController', ['$scope', '$location', '$rootScope', '$timeout', 'Client', function ($scope, $location, $rootScope, $timeout, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastAdmin) $location.path('/'); });

    $scope.client = Client;
    $scope.user = Client.getUserInfo();
    $scope.config = Client.getConfig();
    $scope.installedApps = Client.getInstalledApps();

    $scope.subscription = null;
    $scope.subscriptionBusy = true;

    $scope.openSubscriptionSetup = function () {
        Client.openSubscriptionSetup($scope.subscription || {});
    };

    $scope.prettyProviderName = function (provider) {
        switch (provider) {
        case 'caas': return 'Managed Cloudron';
        default: return provider;
        }
    };

    $scope.update = {
        error: {}, // this is for the dialog
        busy: false,
        checking: false,
        percent: 0,
        message: 'Downloading',
        errorMessage: '', // this shows inline
        taskId: '',
        skipBackup: false,

        checkNow: function () {
            $scope.update.checking = true;

            Client.checkForUpdates(function (error) {
                if (error) Client.error(error);

                $scope.update.checking = false;
            });
        },

        show: function () {
            $scope.update.error.generic = null;
            $scope.update.busy = false;

            $('#updateModal').modal('show');
        },

        stopUpdate: function () {
            Client.stopTask($scope.update.taskId, function (error) {
                if (error) {
                    if (error.statusCode === 409) {
                        $scope.update.errorMessage = 'No update is currently in progress';
                    } else {
                        console.error(error);
                        $scope.update.errorMessage = error.message;
                    }

                    $scope.update.busy = false;

                    return;
                }
            });
        },

        checkStatus: function () {
            Client.getLatestTaskByType('update', function (error, task) {
                if (error) return console.error(error);
                if (!task) return;

                $scope.update.taskId = task.id;
                $scope.update.updateStatus();
            });
        },

        reloadIfNeeded: function () {
            Client.getStatus(function (error, status) {
                if (error) return $scope.error(error);

                if (window.localStorage.version !== status.version) window.location.reload(true);
            });
        },

        updateStatus: function () {
            Client.getTask($scope.update.taskId, function (error, data) {
                if (error) return window.setTimeout($scope.update.updateStatus, 5000);

                if (!data.active) {
                    $scope.update.busy = false;
                    $scope.update.message = '';
                    $scope.update.percent = 100; // indicates that 'result' is valid
                    $scope.update.errorMessage = data.success ? '' : data.error.message;

                    if (!data.errorMessage) $scope.update.reloadIfNeeded(); // assume success

                    return;
                }

                $scope.update.busy = true;
                $scope.update.percent = data.percent;
                $scope.update.message = data.message;

                window.setTimeout($scope.update.updateStatus, 500);
            });
        },

        startUpdate: function () {
            $scope.update.error.generic = null;
            $scope.update.busy = true;
            $scope.update.percent = 0;
            $scope.update.message = '';
            $scope.update.errorMessage = '';

            Client.update({ skipBackup: $scope.update.skipBackup }, function (error, taskId) {
                if (error) {
                    $scope.update.error.generic = error.message;
                    $scope.update.busy = false;
                    return;
                }

                $('#updateModal').modal('hide');

                $scope.update.taskId = taskId;
                $scope.update.updateStatus();
            });
        }
    };

    $scope.timeZone = {
        busy: false,
        success: false,
        error: '',
        timeZone: '',
        currentTimeZone: '',
        availableTimeZones: window.timezones,

        submit: function () {
            if ($scope.timeZone.timeZone === $scope.timeZone.currentTimeZone) return;

            $scope.timeZone.error = '';
            $scope.timeZone.busy = true;
            $scope.timeZone.success = false;

            Client.setTimeZone($scope.timeZone.timeZone.id, function (error) {
                if (error) $scope.timeZone.error = error.message;
                else $scope.timeZone.currentTimeZone = $scope.timeZone.timeZone;

                $scope.timeZone.busy = false;
                $scope.timeZone.success = true;
            });
        }
    };

    $scope.autoUpdate = {
        busy: false,
        pattern: '',
        currentPattern: '',

        submit: function () {
            if ($scope.autoUpdate.pattern === $scope.autoUpdate.currentPattern) return;

            $scope.autoUpdate.busy = true;

            Client.setAppAutoupdatePattern($scope.autoUpdate.pattern, function (error) {
                if (error) Client.error(error);
                else $scope.autoUpdate.currentPattern = $scope.autoUpdate.pattern;

                $timeout(function () {
                    $scope.autoUpdate.busy = false;
                }, 3000);
            });
        }
    };

    function getTimeZone() {
        Client.getTimeZone(function (error, timeZone) {
            if (error) return console.error(error);

            $scope.timeZone.currentTimeZone = window.timezones.find(function (t) { return t.id === timeZone; });
            $scope.timeZone.timeZone = $scope.timeZone.currentTimeZone;
        });
    }

    function getAutoupdatePattern() {
        Client.getAppAutoupdatePattern(function (error, result) {
            if (error) return console.error(error);

            // just keep the UI sane by supporting previous default pattern
            if (result.pattern === '00 30 1,3,5,23 * * *') result.pattern = '00 15 1,3,5,23 * * *';

            $scope.autoUpdate.currentPattern = result.pattern;
            $scope.autoUpdate.pattern = result.pattern;
        });
    }

    function getRegistryConfig() {
        Client.getRegistryConfig(function (error, result) {
            if (error) return console.error(error);

            $scope.registryConfig.currentConfig.serverAddress = result.serverAddress;
            $scope.registryConfig.currentConfig.username = result.username || '';
            $scope.registryConfig.currentConfig.email = result.email || '';
            $scope.registryConfig.currentConfig.password = result.password;
        });
    }

    function getSubscription() {
        $scope.subscriptionBusy = true;

        Client.getSubscription(function (error, result) {
            $scope.subscriptionBusy = false;

            if (error && error.statusCode === 412) return; // not yet registered
            if (error) return console.error(error);

            $scope.subscription = result;
        });
    }

    $scope.registryConfig = {
        busy: false,
        error: null,
        serverAddress: '',
        username: '',
        password: '',
        email: '',
        currentConfig: {},

        reset: function () {
            $scope.registryConfig.busy = false;
            $scope.registryConfig.error = null;

            $scope.registryConfig.serverAddress = $scope.registryConfig.currentConfig.serverAddress;
            $scope.registryConfig.username = $scope.registryConfig.currentConfig.username;
            $scope.registryConfig.email = $scope.registryConfig.currentConfig.email;
            $scope.registryConfig.password = $scope.registryConfig.currentConfig.password;

            $scope.registryConfigForm.$setUntouched();
            $scope.registryConfigForm.$setPristine();
        },

        show: function () {
            $scope.registryConfig.reset();
            $('#registryConfigModal').modal('show');
        },

        submit: function () {
            $scope.registryConfig.busy = true;

            var data = {
                serverAddress: $scope.registryConfig.serverAddress,
                username: $scope.registryConfig.username || '',
                password: $scope.registryConfig.password,
                email: $scope.registryConfig.email || '',
            };

            Client.setRegistryConfig(data, function (error) {
                $scope.registryConfig.busy = false;

                if (error) {
                    $scope.registryConfig.error = error.message;
                    return;
                }

                $('#registryConfigModal').modal('hide');

                getRegistryConfig();
            });
        }
    };

    Client.onReady(function () {
        getAutoupdatePattern();
        getRegistryConfig();
        getTimeZone();

        $scope.update.checkStatus();

        if ($scope.user.isAtLeastOwner) getSubscription();
    });

    // setup all the dialog focus handling
    ['planChangeModal', 'appstoreLoginModal'].forEach(function (id) {
        $('#' + id).on('shown.bs.modal', function () {
            $(this).find("[autofocus]:first").focus();
        });
    });

    $('.modal-backdrop').remove();
}]);
