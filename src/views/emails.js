'use strict';

/* global angular:false */
/* global $:false */

angular.module('Application').controller('EmailsController', ['$scope', '$location', 'Client', function ($scope, $location, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastAdmin) $location.path('/'); });

    $scope.ready = false;
    $scope.config = Client.getConfig();
    $scope.user = Client.getUserInfo();
    $scope.domains = [];

    $scope.pageItemCount = [
        { name: 'Show 20 per page', value: 20 },
        { name: 'Show 50 per page', value: 50 },
        { name: 'Show 100 per page', value: 100 }
    ];

    $scope.activityTypes = [
        { name: 'Bounce', value: 'bounce' },
        { name: 'Deferred', value: 'deferred' },
        { name: 'Delivered', value: 'delivered' },
        { name: 'Denied', value: 'denied' },
        { name: 'Queued', value: 'queued' },
        { name: 'Received', value: 'received' },
    ];

    $scope.activity = {
        busy: true,
        eventLogs: [],
        activeEventLog: null,
        currentPage: 1,
        perPage: 20,
        pageItems: $scope.pageItemCount[0],
        selectedTypes: [],
        search: '',

        refresh: function () {
            $scope.activity.busy = true;

            var types = $scope.activity.selectedTypes.map(function (a) { return a.value; }).join(',');

            Client.getMailEventLogs($scope.activity.search, types, $scope.activity.currentPage, $scope.activity.pageItems.value, function (error, result) {
                if (error) return console.error('Failed to fetch mail eventlogs.', error);

                $scope.activity.busy = false;

                $scope.activity.eventLogs = result;
            });
        },

        showNextPage: function () {
            $scope.activity.currentPage++;
            $scope.activity.refresh();
        },

        showPrevPage: function () {
            if ($scope.activity.currentPage > 1) $scope.activity.currentPage--;
            else $scope.activity.currentPage = 1;
            $scope.activity.refresh();
        },

        showEventLogDetails: function (eventLog) {
            if ($scope.activity.activeEventLog === eventLog) $scope.activity.activeEventLog = null;
            else $scope.activity.activeEventLog = eventLog;
        },

        updateFilter: function (fresh) {
            if (fresh) $scope.activity.currentPage = 1;
            $scope.activity.refresh();
        }
    };

    $scope.testEmail = {
        busy: false,
        error: {},

        mailTo: '',

        domain: null,

        clearForm: function () {
            $scope.testEmail.mailTo = '';
        },

        show: function (domain) {
            $scope.testEmail.error = {};
            $scope.testEmail.busy = false;

            $scope.testEmail.domain = domain;
            $scope.testEmail.mailTo = $scope.user.email;

            $('#testEmailModal').modal('show');
        },

        submit: function () {
            $scope.testEmail.error = {};
            $scope.testEmail.busy = true;

            Client.sendTestMail($scope.testEmail.domain.domain, $scope.testEmail.mailTo, function (error) {
                $scope.testEmail.busy = false;

                if (error) {
                    $scope.testEmail.error.generic = error.message;
                    console.error(error);
                    $('#inputTestMailTo').focus();
                    return;
                }

                $('#testEmailModal').modal('hide');
            });
        }
    };

    function refreshDomainStatuses() {
        $scope.domains.forEach(function (domain) {
            Client.getMailStatusForDomain(domain.domain, function (error, result) {
                if (error) return console.error('Failed to fetch mail status for domain', domain.domain, error);

                domain.status = result;

                domain.statusOk = Object.keys(result).every(function (k) {
                    if (k === 'dns') return Object.keys(result.dns).every(function (k) { return result.dns[k].status; });

                    if (!('status' in result[k])) return true; // if status is not present, the test was not run

                    return result[k].status;
                });
            });

            Client.getMailConfigForDomain(domain.domain, function (error, mailConfig) {
                if (error) return console.error('Failed to fetch mail config for domain', domain.domain, error);

                domain.inbound = mailConfig.enabled;
                domain.outbound = mailConfig.relay.provider !== 'noop';

                // do this even if no outbound since people forget to remove mailboxes
                Client.getMailboxCount(domain.domain, function (error, count) {
                    if (error) return console.error('Failed to fetch mailboxes for domain', domain.domain, error);

                    domain.mailboxCount = count;

                    Client.getMailUsage(domain.domain, function (error, usage) {
                        if (error) return console.error('Failed to fetch usage for domain', domain.domain, error);

                        domain.usage = 0;
                        Object.keys(usage).forEach(function (m) { domain.usage += usage[m].size; });
                    });
                });
            });

        });
    }

    Client.onReady(function () {
        Client.getDomains(function (error, domains) {
            if (error) return console.error('Unable to get domain listing.', error);

            $scope.domains = domains;
            $scope.ready = true;

            if ($scope.user.role === 'owner') $scope.activity.refresh();

            refreshDomainStatuses();
        });
    });

    // setup all the dialog focus handling
    ['testEmailModal'].forEach(function (id) {
        $('#' + id).on('shown.bs.modal', function () {
            $(this).find('[autofocus]:first').focus();
        });
    });

    $('.modal-backdrop').remove();
}]);
