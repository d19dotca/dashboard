'use strict';

/* global async */
/* global angular */
/* global $ */

angular.module('Application').controller('NotificationsController', ['$scope', '$timeout', 'Client', function ($scope, $timeout, Client) {

    $scope.clearAllBusy = false;

    $scope.reboot = {
        busy: false,

        show: function () {
            $scope.reboot.busy = false;
            $('#rebootModal').modal('show');
        },

        submit: function () {
            $scope.reboot.busy = true;

            Client.reboot(function (error) {
                if (error) return Client.error(error);

                $('#rebootModal').modal('hide');

                // trigger refetch to show offline banner
                $timeout(function () { Client.getStatus(function () {}); }, 5000);
            });
        }
    };

    $scope.notifications = {
        notifications: [],
        activeNotification: null,
        busy: true,

        refresh: function () {
            Client.getNotifications(false, 1, 100, function (error, result) {
                if (error) return console.error(error);

                // collapse by default
                result.forEach(function (r) { r.isCollapsed = true; });

                // attempt to parse the message as json
                result.forEach(function (r) {
                    try {
                        r.messageJson = JSON.parse(r.message);
                    } catch (e) {}
                });

                $scope.notifications.notifications = result;

                $scope.notifications.busy = false;
            });
        },

        clicked: function (notification) {
            if ($scope.notifications.activeNotification === notification) return $scope.notifications.activeNotification = null;
            $scope.notifications.activeNotification = notification;
        },

        ack: function (notification, event, callback) {
            callback = callback || function (error) { if (error) console.error(error); };

            if (event) event.stopPropagation();

            Client.ackNotification(notification.id, function (error) {
                if (error) return callback(error);

                $scope.$parent.notificationAcknowledged(notification.id);
                $scope.notifications.refresh();

                callback();
            });
        },

        action: function (notification) {
            if (notification.action) window.location = notification.action;
        },

        clearAll: function () {
            $scope.clearAllBusy = true;

            async.eachSeries($scope.notifications.notifications, function (notification, callback) {
                if (notification.acknowledged) return callback();
                $scope.notifications.ack(notification, null /* no click event */, callback);
            }, function (error) {
                if (error) console.error(error);

                $scope.clearAllBusy = false;
            });
        }
    };

    $scope.notificationExpanding = function (notification) {
        if (!notification.eventId) return;

        notification.busyLoadEvent = true;

        Client.getEvent(notification.eventId, function (error, result) {
            notification.busyLoadEvent = false;

            if (error) return console.error(error);

            notification.event = result;
        });
    };

    Client.onReady(function () {
        $scope.notifications.refresh();
    });

    Client.onReconnect(function () {
        $scope.notifications.refresh();
    });
}]);
