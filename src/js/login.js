'use strict';

/* global angular, $, showdown */

// create main application module
var app = angular.module('Application', []);

app.filter('markdown2html', function () {
    var converter = new showdown.Converter({
        extensions: [ 'targetblank' ],
        simplifiedAutoLink: true,
        strikethrough: true,
        tables: true
    });

    return function (text) {
        return converter.makeHtml(text);
    };
});

// disable sce for footer https://code.angularjs.org/1.5.8/docs/api/ng/service/$sce
app.config(function ($sceProvider) {
    $sceProvider.enabled(false);
});

app.controller('LoginController', ['$scope', '$http', function ($scope, $http) {
    // Stupid angular location provider either wants html5 location mode or not, do the query parsing on my own
    var search = decodeURIComponent(window.location.search).slice(1).split('&').map(function (item) { return item.indexOf('=') === -1 ? [item, true] : [item.slice(0, item.indexOf('=')), item.slice(item.indexOf('=')+1)]; }).reduce(function (o, k) { o[k[0]] = k[1]; return o; }, {});

    $scope.initialized = false;
    $scope.mode = '';
    $scope.busy = false;
    $scope.error = false;
    $scope.status = null;
    $scope.username = '';
    $scope.password = '';
    $scope.totpToken = '';
    $scope.passwordResetIdentifier = '';
    $scope.newPassword = '';
    $scope.newPasswordRepeat = '';
    var API_ORIGIN = '<%= apiOrigin %>' || window.location.origin;

    $scope.onLogin = function () {
        $scope.busy = true;
        $scope.error = false;

        var data = {
            username: $scope.username,
            password: $scope.password,
            totpToken: $scope.totpToken
        };

        function error() {
            $scope.busy = false;
            $scope.error = true;

            $scope.password = '';
            $scope.loginForm.$setPristine();
            setTimeout(function () { $('#inputPassword').focus(); }, 200);
        }

        $http.post(API_ORIGIN + '/api/v1/cloudron/login', data).success(function (data, status) {
            if (status !== 200) return error();

            localStorage.token = data.accessToken;
            window.location.href = search.returnTo || '/';
        }).error(error);
    };

    $scope.onPasswordReset = function () {
        $scope.busy = true;

        var data = {
            identifier: $scope.passwordResetIdentifier
        };

        function done() {
            $scope.busy = false;
            $scope.mode = 'passwordResetDone';
        }

        $http.post(API_ORIGIN + '/api/v1/cloudron/password_reset_request', data).success(done).error(done);
    };

    $scope.onNewPassword = function () {
        $scope.busy = true;

        var data = {
            resetToken: search.resetToken,
            password: $scope.newPassword
        };

        function error(status) {
            console.log('error', status)
            $scope.busy = false;

            if (status === 401) $scope.error = 'Invalid reset token';
            else if (status === 409) $scope.error = 'Ask your admin for an invite link first';
            else $scope.error = 'Unknown error';
        }

        $http.post(API_ORIGIN + '/api/v1/cloudron/password_reset', data).success(function (data, status) {
            if (status !== 202) return error(status);

            // set token to autologin
            localStorage.token = data.accessToken;

            $scope.mode = 'newPasswordDone';
        }).error(function (data, status) {
            error(status);
        });
    };

    $scope.showPasswordReset = function () {
        window.document.title = 'Password Reset';
        $scope.mode = 'passwordReset';
        $scope.passwordResetIdentifier = '';
        setTimeout(function () { $('#inputPasswordResetIdentifier').focus(); }, 200);
    };

    $scope.showLogin = function () {
        if ($scope.status) window.document.title = $scope.status.cloudronName + ' Login';
        $scope.mode = 'login';
        $scope.error = false;
        setTimeout(function () { $('#inputUsername').focus(); }, 200);
    };

    $scope.showNewPassword = function () {
        window.document.title = 'Set New Password';
        $scope.mode = 'newPassword';
        setTimeout(function () { $('#inputNewPassword').focus(); }, 200);
    };

    $http.get(API_ORIGIN + '/api/v1/cloudron/status').success(function (data, status) {
        $scope.initialized = true;

        if (status !== 200) return;

        if ($scope.mode === 'login') window.document.title = data.cloudronName + ' Login';
        $scope.status = data;
    }).error(function () {
        $scope.initialized = false;
    });

    // Init into the correct view
    if (search.passwordReset) $scope.showPasswordReset();
    else if (search.resetToken) $scope.showNewPassword();
    else $scope.showLogin();
}]);
