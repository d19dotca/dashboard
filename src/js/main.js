'use strict';

/* global angular */
/* global $ */

angular.module('Application').controller('MainController', ['$scope', '$route', '$timeout', '$location', 'Client', function ($scope, $route, $timeout, $location, Client) {
    $scope.initialized = false; // used to animate the UI
    $scope.user = Client.getUserInfo();
    $scope.installedApps = Client.getInstalledApps();
    $scope.config = {};
    $scope.client = Client;
    $scope.subscription = {};
    $scope.notifications = [];
    $scope.hideNavBarActions = $location.path() === '/logs';

    $scope.isActive = function (url) {
        if (!$route.current) return false;
        return $route.current.$$route.originalPath.indexOf(url) === 0;
    };

    $scope.logout = function (event) {
        event.stopPropagation();
        $scope.initialized = false;
        Client.logout();
    };

    $scope.openSubscriptionSetup = function () {
        Client.openSubscriptionSetup($scope.subscription);
    };

    // NOTE: this function is exported and called from the appstore.js
    $scope.updateSubscriptionStatus = function () {
        if (!Client.getUserInfo().isAtLeastAdmin) return;

        Client.getSubscription(function (error, subscription) {
            if (error && error.statusCode === 412) return; // ignore if not yet registered
            if (error && error.statusCode === 402) return; // ignore if not yet registered
            if (error) return console.error(error);

            $scope.subscription = subscription;
        });
    };

    function refreshNotifications(poll) {
        Client.getNotifications(false, 1, 100, function (error, results) {
            if (error) console.error(error);
            else $scope.notifications = results;

            if (poll) $timeout(refreshNotifications, 60 * 1000);
        });
    }

    // update state of acknowledged notification
    $scope.notificationAcknowledged = function (notificationId) {
        // remove notification from list
        $scope.notifications = $scope.notifications.filter(function (n) { return n.id !== notificationId; });
    };

    function init() {
        Client.getStatus(function (error, status) {
            if (error) return Client.initError(error, init);

            // WARNING if anything about the routing is changed here test these use-cases:
            //
            // 1. Caas
            // 3. selfhosted restore
            // 4. local development with gulp develop

            if (!status.activated) {
                console.log('Not activated yet, redirecting', status);
                if (status.restore.active || status.restore.errorMessage) { // show the error message in restore page
                    window.location.href = '/restore.html';
                } else {
                    window.location.href = status.adminFqdn ? '/setup.html' : '/setupdns.html';
                }
                return;
            }

            // support local development with localhost check
            if (window.location.hostname !== status.adminFqdn && window.location.hostname !== 'localhost') {
                // user is accessing by IP or by the old admin location (pre-migration)
                window.location.href = '/setupdns.html';
                return;
            }

            // check version and force reload if needed
            if (!localStorage.version) {
                localStorage.version = status.version;
            } else if (localStorage.version !== status.version) {
                localStorage.version = status.version;
                window.location.reload(true);
            }

            console.log('Running dashboard version ', localStorage.version);

            // get user profile as the first thing. this populates the "scope" and affects subsequent API calls
            Client.refreshUserInfo(function (error) {
                if (error) return Client.initError(error, init);

                Client.refreshConfig(function (error) {
                    if (error) return Client.initError(error, init);

                    Client.refreshInstalledApps(function (error) {
                        if (error) return Client.initError(error, init);

                        // now mark the Client to be ready
                        Client.setReady();

                        $scope.config = Client.getConfig();

                        $scope.initialized = true;

                        if (Client.getConfig().mandatory2FA && !Client.getUserInfo().twoFactorAuthenticationEnabled) {
                            $location.path('/profile').search({ setup2fa: true });
                            return;
                        }

                        refreshNotifications(true);

                        $scope.updateSubscriptionStatus();
                    });
                });
            });
        });
    }

    Client.onConfig(function (config) {
        if (config.cloudronName) {
            document.title = config.cloudronName;
        }
    });

    Client.onReconnect(function () {
        refreshNotifications(false);
    });

    init();

    // setup all the dialog focus handling
    ['updateModal'].forEach(function (id) {
        $('#' + id).on('shown.bs.modal', function () {
            $(this).find('[autofocus]:first').focus();
        });
    });
}]);
